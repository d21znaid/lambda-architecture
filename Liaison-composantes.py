from __future__ import print_function
import sys
from pyspark.sql import SparkSession
from pyspark.sql.functions import explode,split,from_json,col
from pyspark import SparkContext, StorageLevel
from pyspark.streaming import StreamingContext
import os
from pyspark.sql.types import StructType,StructField,TimestampType,LongType,StringType
#os.environ['PYSPARK_SUBMIT_ARGS'] = '--packages org.apache.spark:spark-streaming-kafka-0-10_2.12:3.0.1,org.apache.spark:spark-sql-kafka-0-10_2.12:3.0.1 pyspark-shell'

if __name__ == "__main__":
        #if len(sys.argv) != 3:
        #       print("Usage: projet_data_Iheb_Dhia <hostname> <port>",file=stderr)
        #       sys.exit(-1)
        twitterSchema = StructType([StructField("start",TimestampType()),StructField("end",TimestampType()),StructField("tweet_count",LongType()),StructField("company_name",StringType())])
        hdfs_directory = "hdfs://pi-node18:9000/home/pi/data/"
        primary_key= "company_name"
        spark = SparkSession.builder.appName("Kafka-SparkStreaming").getOrCreate()

        # Create DataFrame representing the stream of input lines from connection to localhost:9999
        #df = spark.readStream.format("kafka").option("kafka.bootstrap.servers", "10.29.239.99:9092").option("subscribe", "twitter").load()
        #df= df.withColumn("value",df["value"].cast(StringType()))
        #df = df.select(from_json(df["value"],twitterSchema).alias("json")).selectExpr("json.start","json.end","json.tweet_count","json.company_name")
        #agg_df= df.groupBy(primary_key).count()
        #print(agg_df)
        #df.writeStream.format("console").start().awaitTermination()
        #agg_df.writeStream.outputMode("complete").format("console").option("truncate",False).start().awaitTermation()
        #df.printSchema()
        #df.map(lambda x: x[1])
        #df.pprint()
        #spark.start()
        #spark.awaitTermination()
        # create SparkSession with the configurations and packages

        df = spark \
        .readStream \
        .format('kafka')\
        .option("kafka.bootstrap.servers", "pi-node03:9092") \
        .option("subscribe", "projectTwitter") \
        .option("startingOffsets", "earliest") \
        .load() \
        .selectExpr("CAST(value AS STRING)","timestamp")
        df1=df.select(from_json(col("value"), twitterSchema).alias("companyDetail"),"timestamp")
        df2=df1.select("companyDetail.*","timestamp")
        df3=df2.withWatermark("timestamp","1 minute").groupBy("company_name").agg({"tweet_count":'sum'}).select("company_name",\
        col("sum(tweet_count)").alias("tweet_count"))
        #df3.printSchema()
        '''
        def parse_data_from_kafka_message(sdf, schema):
                assert sdf.isStreaming == True, "DataFrame doesn't receive streaming data"
                col = split(sdf['value'], ',')
                #split attributes to nested array in one Column
                #now expand col to multiple top-level columns
                for idx, field in enumerate(schema):
                        sdf = sdf.withColumn(field.name, col.getItem(idx).cast(field.dataType))
                return sdf.select([field.name for field in schema])
        df=parse_data_from_kafka_message(df, twitterSchema)
        #summed_df = df.groupBy("company_name").agg(sum("tweet_count").alias("total_tweets"))
        query= df.groupBy("company_name").sum("tweet_count")
        '''
        #df3.write.format("org.apache.spark.sql.cassandra").mode('append').options(table="projet", keyspace="tweets").save()
        def write_to_cassandra(batch_df, batch_id):
                # Write the output DataFrame to Cassandra
                batch_df.write \
                .format("org.apache.spark.sql.cassandra") \
                .options(table="tweets", keyspace="projet") \
                .mode("append") \
                .save()
        df3 \
        .filter(col("company_name").isNotNull())\
        .writeStream \
        .foreachBatch(write_to_cassandra)\
        .outputMode("update") \
        .start()
        #query.awaitTermination()

        df2 \
        .writeStream \
        .format("parquet") \
        .option("checkpointLocation", "hdfs://pi-node18:9000/home/pi/checkpoint") \
        .option("path", hdfs_directory) \
        .start()
        spark.streams.awaitAnyTermination()