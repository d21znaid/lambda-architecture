from __future__ import print_function
import sys
from pyspark.sql import SparkSession
from pyspark.sql.functions import explode,split,from_json,col,to_timestamp, current_timestamp, trunc, date_sub,lit
from pyspark import SparkContext, StorageLevel
from pyspark.streaming import StreamingContext
import os
from pyspark.sql.types import StructType,StructField,TimestampType,LongType,StringType
from pyspark.sql.functions import unix_timestamp
from datetime import datetime
#os.environ['PYSPARK_SUBMIT_ARGS'] = '--packages org.apache.spark:spark-streaming-kafka-0-10_2.12:3.0.1,org.apache.spark:spark-sql-kafka-0-10_2.12:3.0.1 pyspark-shell'

if __name__ == "__main__":
        spark = SparkSession.builder.appName("ReadFromHDFS").getOrCreate()

        # HDFS path where the Parquet data is stored
        hdfs_path = "hdfs://pi-node18:9000/home/pi/data"

        # Read the Parquet data into a DataFrame
        df = spark.read.format("parquet").load(hdfs_path)
        df= df.filter(col("company_name").isNotNull())
        df = df.withColumn("timestamp", to_timestamp("timestamp"))
        current_ts = trunc(current_timestamp(), "day")
        last_midnight_ts = date_sub(current_ts, 1)
        df_filtered = df.filter((col("timestamp") >= last_midnight_ts) & (col("timestamp") <= current_timestamp()))
        df_filtered.show()
        df1= df.groupBy("company_name").agg({"tweet_count":'avg'}).select("company_name",col("avg(tweet_count)").alias("mean_tweets_per_day"))
        current_ts = datetime.now() # Get current timestamp as datetime object
        current_ts_str = current_ts.strftime("%Y-%m-%d") # Convert datetime object to string
        current_ts_ts = lit(current_ts_str).cast("timestamp") # Convert string to timestamp

        df1 = df1.withColumn("timestamp", current_ts_ts)
        # print the schema of the DataFrame
        df1.printSchema()

        # show the content of the DataFrame
        df1.show()
        df1.write\
        .format("org.apache.spark.sql.cassandra")\
        .option("keyspace", "projet")\
        .option("table", "tweetsavg")\
        .save(mode="append")
        # Stop the SparkSession
        spark.stop()